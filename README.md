# Plazma - Amatör Bilgisayar Kültürü



## Kaynaklar

* [Web Archive - Plazma Dergi Anasayfa][webarc-plazma]
* [Scene.org Archive - Plazma Dergi][scene-plazma-arc]
* [FTP Archive - Plazma Dergi][plazma-ftp-arc]
* [Bronx ve demo grubu tarihçesi][bronx]

[webarc-plazma]: https://web.archive.org/web/20190107123308/http://plazma-dergi.org/
[scene-plazma-arc]: https://http.hu.scene.org/mags/plazma/
[plazma-ftp-arc]: ftp://ftp.icm.edu.pl/pub/demos/mags/plazma/
[bronx]: http://www.sadecebirmuze.com/rop/bronx.html
